package com.twuc.webApp.pojo;

public class CheckPojo {
    private Integer operandLeft;
    private Integer operandRight;
    private String operation;
    private Integer expectedResult;
    private String checkType = "=";

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public void setOperandLeft(Integer operandLeft) {
        this.operandLeft = operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public void setOperandRight(Integer operandRight) {
        this.operandRight = operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(Integer expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public String toString() {
        return "CheckPojo{" +
                "operandLeft=" + operandLeft +
                ", operandRight=" + operandRight +
                ", operation='" + operation + '\'' +
                ", expectedResult=" + expectedResult +
                ", checkType='" + checkType + '\'' +
                '}';
    }
}
