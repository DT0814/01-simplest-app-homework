package com.twuc.webApp.pojo;

public class CheckResult {
    private Boolean correct;

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public CheckResult(Boolean correct) {
        this.correct = correct;
    }

    public CheckResult() {
    }

    @Override
    public String toString() {
        return "{" +
                "\"correct\":" + correct +
                '}';
    }
}
