package com.twuc.webApp.service;

import com.twuc.webApp.pojo.CheckPojo;
import org.springframework.stereotype.Service;

/**
 * @author tao.dong
 */
@Service
public class NumberService {
    public String plus(Integer start, Integer end, Boolean json) {
        return getResult(start, end, '+', json);
    }

    public String multiply(Integer start, Integer end, Boolean json) {
        return getResult(start, end, '*', json);
    }

    private String getResult(Integer start, Integer end, Character symbol, Boolean json) {
        StringBuilder sb = new StringBuilder();
        if (json) {
            sb.append('[');
        }
        for (int i = start; i <= end; i++) {
            if (json) {
                sb.append('[');
            }
            for (int j = start; j <= i; j++) {
                switch (symbol) {
                    case '*':
                        sb.append(getArithmetic(i, j, symbol, i * j,json));
                        break;
                    case '+':
                        sb.append(getArithmetic(i, j, symbol, i + j,json));
                    default:
                        break;
                }
            }
            if (json) {
                sb.append(']');
            }
            sb.append("\n");
        }
        if (json) {
            sb.append(']');
        }
        return sb.toString();
    }

    private String getArithmetic(int first, int second, char symbol, int result, boolean json) {
        StringBuilder sb = new StringBuilder();
        sb.append(first);
        sb.append(symbol);
        sb.append(second);
        sb.append('=');
        sb.append(result);
        if (first != second) {
            if (json) {
                sb.append(',');
            } else {
                sb.append(" ");
            }
            if (result < 9) {
                if (!json) {
                    sb.append(" ");

                }
            }
        }
        return sb.toString();
    }

    public boolean check(CheckPojo checkPojo) {
        int res;
        switch (checkPojo.getOperation()) {
            case "+":
                res = checkPojo.getOperandRight() + checkPojo.getOperandLeft();
                return checkResult(checkPojo.getCheckType(), res, checkPojo.getExpectedResult());
            case "*":
                res = checkPojo.getOperandRight() * checkPojo.getOperandLeft();
                return checkResult(checkPojo.getCheckType(), res, checkPojo.getExpectedResult());
            default:
                return false;
        }
    }

    private boolean checkResult(String checkType, Integer result, Integer expected) {
        switch (checkType) {
            case ">":
                return result > expected;
            case "<":
                return result < expected;
            case "=":
                return result.equals(expected);
            default:
                return false;
        }
    }
}
