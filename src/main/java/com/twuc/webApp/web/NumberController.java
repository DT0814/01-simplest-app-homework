package com.twuc.webApp.web;

import com.twuc.webApp.pojo.CheckPojo;
import com.twuc.webApp.pojo.CheckResult;
import com.twuc.webApp.service.NumberService;
import com.twuc.webApp.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/tables/")
public class NumberController {

    @Autowired
    private NumberService numberService;


    @GetMapping("plus")
    public String plus(@RequestParam(name = "start", defaultValue = "1") Integer start,
                       @RequestParam(name = "end", defaultValue = "9") Integer end,
                       @RequestParam(name = "json", defaultValue = "false") Boolean json) {
        if (checkPlusAndMultiplyParam(start, end)) {
            return Result.fail(400);
        }
        String res = numberService.plus(start, end,json);
        return res;
    }

    @GetMapping("multiply")
    public String multiply(@RequestParam(name = "start", defaultValue = "1") Integer start,
                           @RequestParam(name = "end", defaultValue = "9") Integer end,
                           @RequestParam(name = "json", defaultValue = "false") Boolean json) {
        if (checkPlusAndMultiplyParam(start, end)) {
            return Result.fail(400);
        }
        String res = numberService.multiply(start, end,json);
        return res;
    }

    @PostMapping("check")
    public String check(@RequestBody CheckPojo checkPojo) {
        if (checkEqualParam(checkPojo)) {
            return Result.fail(400);
        }
        boolean res = numberService.check(checkPojo);
        return new CheckResult(res).toString();
    }

    private boolean checkEqualParam(CheckPojo checkPojo) {
        if (null == checkPojo) {
            return true;
        }
        if (null == checkPojo.getOperation()) {
            return true;
        }
        if (checkNumber(checkPojo.getOperandLeft(), checkPojo.getOperandRight(), checkPojo.getExpectedResult())) {
            return true;
        }
        if (!"*".equals(checkPojo.getOperation()) && !"+".equals(checkPojo.getOperation())) {
            return true;
        }
        return false;
    }

    private boolean checkNumber(Integer operandLeft, Integer expectedResult, Integer operandRight) {
        return null == operandLeft || null == expectedResult || null == operandRight;
    }

    private boolean checkPlusAndMultiplyParam(Integer start, Integer end) {
        return start > end || start > 9 || start < 1 || end > 9 || end < 1;
    }

}
