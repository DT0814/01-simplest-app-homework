package com.twuc.webApp.util;

public class Result {
    private Integer StatusCode;

    public Result(Integer statusCode) {
        StatusCode = statusCode;
    }

    public Result() {
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    public static String fail(Integer statusCode) {
        return "Status Code:" + statusCode;
    }
}
